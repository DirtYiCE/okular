/*
    SPDX-FileCopyrightText: 2013 Fabio D 'Urso <fabiodurso@hotmail.it>

    Work sponsored by the LiMux project of the city of Munich:
    SPDX-FileCopyrightText: 2017 Klarälvdalens Datakonsult AB a KDAB Group company <info@kdab.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include <QMimeDatabase>
#include <QTemporaryFile>
#include <QTest>

#include <threadweaver/queue.h>

#include "../core/annotations.h"
#include "../core/document.h"
#include "../core/document_p.h"
#include "../core/generator.h"
#include "../core/observer.h"
#include "../core/page.h"
#include "../core/rotationjob_p.h"
#include "../settings_core.h"

class DocumentTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testCloseDuringRotationJob();
    void testDiff_data();
    void testDiff();
};

// Test that we don't crash if the document is closed while a RotationJob
// is enqueued/running
void DocumentTest::testCloseDuringRotationJob()
{
    Okular::SettingsCore::instance(QStringLiteral("documenttest"));
    Okular::Document *m_document = new Okular::Document(nullptr);
    const QString testFile = QStringLiteral(KDESRCDIR "data/file1.pdf");
    QMimeDatabase db;
    const QMimeType mime = db.mimeTypeForFile(testFile);

    Okular::DocumentObserver *dummyDocumentObserver = new Okular::DocumentObserver();
    m_document->addObserver(dummyDocumentObserver);

    m_document->openDocument(testFile, QUrl(), mime);
    m_document->setRotation(1);

    // Tell ThreadWeaver not to start any new job
    ThreadWeaver::Queue::instance()->suspend();

    // Request a pixmap. A RotationJob will be enqueued but not started
    Okular::PixmapRequest *pixmapReq = new Okular::PixmapRequest(dummyDocumentObserver, 0, 100, 100, qApp->devicePixelRatio(), 1, Okular::PixmapRequest::NoFeature);
    m_document->requestPixmaps({pixmapReq});

    // Delete the document
    delete m_document;

    // Resume job processing and wait for the RotationJob to finish
    ThreadWeaver::Queue::instance()->resume();
    ThreadWeaver::Queue::instance()->finish();
    qApp->processEvents();

    delete dummyDocumentObserver;
}

void DocumentTest::testDiff_data()
{
    QTest::addColumn<QString>("oldVal");
    QTest::addColumn<QString>("newVal");
    QTest::addColumn<QString>("expectedDiff");

    QTest::addRow("empty") << ""
                           << ""
                           << "";
    QTest::addRow("a") << ""
                       << "a"
                       << "a";
    QTest::addRow("ab") << "a"
                        << "b"
                        << "b";
    QTest::addRow("ab2") << "a"
                         << "ab"
                         << "b";
    QTest::addRow("kaesekuchen") << "Käse"
                                 << "Käsekuchen"
                                 << "kuchen";
    QTest::addRow("replace") << "kuchen"
                             << "wurst"
                             << "wurst";
    QTest::addRow("okular") << "Oku"
                            << "Okular"
                            << "lar";
    QTest::addRow("removal1") << "a"
                              << ""
                              << "";
    QTest::addRow("removal2") << "ab"
                              << "a"
                              << "";
    QTest::addRow("unicode") << "☮🤌"
                             << "☮🤌❤️"
                             << "❤️";
}

void DocumentTest::testDiff()
{
    QFETCH(QString, oldVal);
    QFETCH(QString, newVal);
    QFETCH(QString, expectedDiff);

    QCOMPARE(Okular::DocumentPrivate::diff(oldVal, newVal), expectedDiff);
}

QTEST_MAIN(DocumentTest)
#include "documenttest.moc"
